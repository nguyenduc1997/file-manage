package com.example.myapplication

import android.content.Context
import android.os.Build
import com.example.myapplication.file_operation.filesystem.OpenMode
import jcifs.smb.SmbException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException

object MakeDirectoryOperation {

    private val log: Logger = LoggerFactory.getLogger(MakeDirectoryOperation::class.java)

    /**
     * Create a folder. The folder may even be on external SD card for Kitkat.
     *
     * @param file The folder to be created.
     * @return True if creation was successful.
     */
    @JvmStatic
    @Deprecated("use {@link #mkdirs(Context, HybridFile)}")
    fun mkdir(file: File?, context: Context): Boolean {
        if (file == null) return false
        if (file.exists()) {
            // nothing to create.
            return file.isDirectory
        }

        // Try the normal way
        if (file.mkdirs()) {
            return true
        }

        // Try with Storage Access Framework.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
            ExternalSdCardOperation.isOnExtSdCard(file, context)
        ) {
            val document = ExternalSdCardOperation.getDocumentFile(file, true, context)
            document ?: return false
            // getDocumentFile implicitly creates the directory.
            return document.exists()
        }

        // Try the Kitkat workaround.
        return if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            try {
                MediaStoreHack.mkdir(context, file)
            } catch (e: IOException) {
                false
            }
        } else false
    }

    @JvmStatic
    fun mkdirs(context: Context, file: HybridFile): Boolean {
        var isSuccessful = true
        when (file.mode) {
            OpenMode.SMB ->
                try {
                    val smbFile = file.smbFile
                    smbFile.mkdirs()
                } catch (e: SmbException) {
                    log.warn("failed to make directory in smb", e)
                    isSuccessful = false
                }
            OpenMode.OTG -> {
                val documentFile = OTGUtil.getDocumentFile(file.getPath(), context, true)
                isSuccessful = documentFile != null
            }
            OpenMode.FILE -> isSuccessful = mkdir(File(file.getPath()), context)
            else -> isSuccessful = true
        }
        return isSuccessful
    }
}
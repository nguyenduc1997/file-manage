package com.example.myapplication;

import androidx.room.TypeConverter;

public class EncryptedStringTypeConverter {

    private static final String TAG = EncryptedStringTypeConverter.class.getSimpleName();

    @TypeConverter
    public static StringWrapper toPassword(String encryptedStringEntryInDb) {
        try {
            return new StringWrapper(
                    PasswordUtil.INSTANCE.decryptPassword(AppConfig.getInstance(), encryptedStringEntryInDb));
        } catch (Exception e) {
            android.util.Log.e(TAG, "Error decrypting password", e);
            return new StringWrapper(encryptedStringEntryInDb);
        }
    }

    @TypeConverter
    public static String fromPassword(StringWrapper unencryptedPasswordString) {
        try {
            return PasswordUtil.INSTANCE.encryptPassword(
                    AppConfig.getInstance(), unencryptedPasswordString.value);
        } catch (Exception e) {
            android.util.Log.e(TAG, "Error encrypting password", e);
            return unencryptedPasswordString.value;
        }
    }
}

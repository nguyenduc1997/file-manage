package com.example.myapplication

import android.content.Context
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import java.io.InputStream

class TarExtractor(
    context: Context,
    filePath: String,
    outputPath: String,
    listener: Extractor.OnUpdate,
    updatePosition: UpdatePosition
) : AbstractCommonsArchiveExtractor(
    context,
    filePath,
    outputPath,
    listener,
    updatePosition
) {

    override fun createFrom(inputStream: InputStream): TarArchiveInputStream =
        runCatching {
            TarArchiveInputStream(inputStream)
        }.getOrElse {
            throw BadArchiveNotice(it)
        }
}
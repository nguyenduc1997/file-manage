package com.example.myapplication

import net.schmizz.sshj.DefaultConfig
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.Security

class CustomSshJConfig : DefaultConfig() {

    companion object {
        /**
         * This is where we different from the original AndroidConfig. Found it only work if we remove
         * BouncyCastle bundled with Android before registering our BouncyCastle provider
         */
        @JvmStatic
        fun init() {
            Security.removeProvider("BC")
            Security.insertProviderAt(BouncyCastleProvider(), 0)
        }
    }
}
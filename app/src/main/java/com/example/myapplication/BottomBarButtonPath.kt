package com.example.myapplication

import androidx.annotation.DrawableRes

interface BottomBarButtonPath {
    /**
     * This allows the fragment to change the path represented in the BottomBar directly
     */
    fun changePath(path: String)
    val path: String?

    @get:DrawableRes
    val rootDrawable: Int
}
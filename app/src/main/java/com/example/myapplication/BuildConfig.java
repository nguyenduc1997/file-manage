package com.example.myapplication;

public final class BuildConfig {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "com.example.myapplication";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "fdroid";
    public static final int VERSION_CODE = 110;
    public static final String VERSION_NAME = "3.7.2";
    // Field from build type: debug
    public static final String CRYPTO_IV = "LxbHiJhhUXcj";
    // Field from build type: debug
    public static final String FTP_SERVER_KEYSTORE_PASSWORD = "vishal007";
    // Field from product flavor: fdroid
    public static final boolean IS_VERSION_FDROID = true;
}

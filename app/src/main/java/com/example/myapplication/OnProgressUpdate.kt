package com.example.myapplication

interface OnProgressUpdate<T> {
    @Suppress("UndocumentedPublicFunction")
    fun onUpdate(data: T)
}
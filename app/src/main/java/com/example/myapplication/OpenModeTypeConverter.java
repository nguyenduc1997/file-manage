package com.example.myapplication;

import androidx.room.TypeConverter;

import com.example.myapplication.file_operation.filesystem.OpenMode;

public class OpenModeTypeConverter {

    @TypeConverter
    public static int fromOpenMode(OpenMode from) {
        return from.ordinal();
    }

    @TypeConverter
    public static OpenMode fromDatabaseValue(int from) {
        return OpenMode.getOpenMode(from);
    }
}
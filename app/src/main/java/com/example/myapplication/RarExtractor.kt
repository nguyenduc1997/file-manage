package com.example.myapplication

import android.content.Context

class RarExtractor(
    context: Context,
    filePath: String,
    outputPath: String,
    listener: OnUpdate,
    updatePosition: UpdatePosition
) : Extractor(context, filePath, outputPath, listener, updatePosition) {
    override fun extractWithFilter(filter: Filter) {
        // no-op
    }
}
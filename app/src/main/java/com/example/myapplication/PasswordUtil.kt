package com.example.myapplication

import android.content.Context
import android.os.Build
import android.util.Base64
import androidx.annotation.RequiresApi
import java.io.IOException
import java.security.GeneralSecurityException
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.IvParameterSpec

object PasswordUtil {

    // 12 byte long IV supported by android for GCM
    private const val IV = BuildConfig.CRYPTO_IV

    /** Helper method to encrypt plain text password  */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Throws(
        GeneralSecurityException::class,
        IOException::class
    )
    private fun aesEncryptPassword(plainTextPassword: String): String? {
        val cipher = Cipher.getInstance(CryptUtil.ALGO_AES)
        val gcmParameterSpec = GCMParameterSpec(128, IV.toByteArray())
        cipher.init(Cipher.ENCRYPT_MODE, SecretKeygen.getSecretKey(), gcmParameterSpec)
        val encodedBytes = cipher.doFinal(plainTextPassword.toByteArray())
        return Base64.encodeToString(encodedBytes, Base64.DEFAULT)
    }

    /** Helper method to decrypt cipher text password  */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Throws(
        GeneralSecurityException::class,
        IOException::class
    )
    private fun aesDecryptPassword(cipherPassword: String): String {
        val cipher = Cipher.getInstance(CryptUtil.ALGO_AES)
        val gcmParameterSpec = GCMParameterSpec(128, IV.toByteArray())
        cipher.init(Cipher.DECRYPT_MODE, SecretKeygen.getSecretKey(), gcmParameterSpec)
        val decryptedBytes = cipher.doFinal(Base64.decode(cipherPassword, Base64.DEFAULT))
        return String(decryptedBytes)
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Throws(
        GeneralSecurityException::class,
        IOException::class
    )
    private fun rsaEncryptPassword(context: Context, password: String): String? {
        val cipher = Cipher.getInstance(CryptUtil.ALGO_AES)
        val ivParameterSpec = IvParameterSpec(IV.toByteArray())
        cipher.init(Cipher.ENCRYPT_MODE, SecretKeygen.getSecretKey(), ivParameterSpec)
        return Base64.encodeToString(cipher.doFinal(password.toByteArray()), Base64.DEFAULT)
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Throws(
        GeneralSecurityException::class,
        IOException::class
    )
    private fun rsaDecryptPassword(context: Context, cipherText: String): String {
        val cipher = Cipher.getInstance(CryptUtil.ALGO_AES)
        val ivParameterSpec = IvParameterSpec(IV.toByteArray())
        cipher.init(Cipher.DECRYPT_MODE, SecretKeygen.getSecretKey(), ivParameterSpec)
        val decryptedBytes = cipher.doFinal(Base64.decode(cipherText, Base64.DEFAULT))
        return String(decryptedBytes)
    }

    /** Method handles encryption of plain text on various APIs  */
    @Throws(GeneralSecurityException::class, IOException::class)
    fun encryptPassword(context: Context, plainText: String): String? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            aesEncryptPassword(plainText)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            rsaEncryptPassword(context, plainText)
        } else plainText
    }

    /** Method handles decryption of cipher text on various APIs  */
    @Throws(GeneralSecurityException::class, IOException::class)
    fun decryptPassword(context: Context, cipherText: String): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            aesDecryptPassword(cipherText)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            rsaDecryptPassword(context, cipherText)
        } else cipherText
    }
}

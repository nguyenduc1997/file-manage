package com.example.myapplication;

public interface UpdatePosition {
    public void updatePosition(long toAdd);
}
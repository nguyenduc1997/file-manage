package com.example.myapplication

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ComputerParcelable(val addr: String?, val name: String?) : Parcelable {
    override fun toString(): String = String.format("%s [%s]", name, addr)
}
package com.example.myapplication;

import static com.example.myapplication.ExplorerDatabase.COLUMN_PATH;
import static com.example.myapplication.ExplorerDatabase.TABLE_ENCRYPTED;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface EncryptedEntryDao {

    @Insert
    Completable insert(EncryptedEntry entry);

    @Query("SELECT * FROM " + TABLE_ENCRYPTED + " WHERE " + COLUMN_PATH + " = :path")
    Single<EncryptedEntry> select(String path);

    @Update
    Completable update(EncryptedEntry entry);

    @Transaction
    @Query("DELETE FROM " + TABLE_ENCRYPTED + " WHERE " + COLUMN_PATH + " = :path")
    Completable delete(String path);

    @Query("SELECT * FROM " + TABLE_ENCRYPTED)
    Single<List<EncryptedEntry>> list();
}


package com.example.myapplication;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

public class MainFragment extends Fragment implements BottomBarButtonPath, ViewTreeObserver.OnGlobalLayoutListener, AdjustListViewForTv<ItemViewHolder>{
    public RecyclerAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView listView;
    private SharedPreferences sharedPref;
    private HashMap<String, Bundle> scrolls = new HashMap<>();
    private View rootView;

}

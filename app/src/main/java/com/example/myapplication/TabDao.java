package com.example.myapplication;

import static com.example.myapplication.ExplorerDatabase.COLUMN_TAB_NO;
import static com.example.myapplication.ExplorerDatabase.TABLE_TAB;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface TabDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertTab(Tab tab);

    @Transaction
    @Query("DELETE FROM " + TABLE_TAB)
    Completable clear();

    @Query("SELECT * FROM " + TABLE_TAB + " WHERE " + COLUMN_TAB_NO + " = :tabNo")
    Single<Tab> find(int tabNo);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Tab tab);

    @Query("SELECT * FROM " + TABLE_TAB)
    Single<List<Tab>> list();
}
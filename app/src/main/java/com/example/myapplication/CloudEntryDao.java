package com.example.myapplication;

import static com.example.myapplication.ExplorerDatabase.COLUMN_CLOUD_SERVICE;
import static com.example.myapplication.ExplorerDatabase.TABLE_CLOUD_PERSIST;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface CloudEntryDao {

    @Insert
    Completable insert(CloudEntry entry);

    @Query(
            "SELECT * FROM " + TABLE_CLOUD_PERSIST + " WHERE " + COLUMN_CLOUD_SERVICE + " = :serviceType")
    Single<CloudEntry> findByServiceType(int serviceType);

    @Query("SELECT * FROM " + TABLE_CLOUD_PERSIST)
    Single<List<CloudEntry>> list();

    @Update
    Completable update(CloudEntry entry);

    @Delete
    Completable delete(CloudEntry entry);
}

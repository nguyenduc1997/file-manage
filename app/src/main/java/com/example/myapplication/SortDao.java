package com.example.myapplication;

import static com.example.myapplication.ExplorerDatabase.COLUMN_PATH;
import static com.example.myapplication.ExplorerDatabase.TABLE_SORT;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface SortDao {

    @Insert
    Completable insert(Sort entity);

    @Query("SELECT * FROM " + TABLE_SORT + " WHERE " + COLUMN_PATH + " = :path")
    Single<Sort> find(String path);

    @Transaction
    @Query("DELETE FROM " + TABLE_SORT + " WHERE " + COLUMN_PATH + " = :path")
    Completable clear(String path);

    @Update
    Completable update(Sort entity);
}
